package com.task.service;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.task.entity.InviteEntity;
import com.task.repository.InviteReposiotry;

@Service
public class InviteServiceImpl {

	@Autowired
	private InviteReposiotry inviteReposiotry;

	public void add(UUID uuid, Long userId) {
		try {

			InviteEntity inviteEntity = new InviteEntity();
			inviteEntity.setUserId(userId);
			inviteEntity.setCode(uuid);

			this.inviteReposiotry.save(inviteEntity);

		} catch (Exception e) {
		}

	}
}
package com.task.service;

import java.util.ArrayList;

import org.apache.kafka.common.errors.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.task.entity.Status;
import com.task.entity.SubTopicMasterEntity;
import com.task.entity.TopicEntity;
import com.task.entity.UserEntity;
import com.task.entity.UserTopicAddDto;
import com.task.entity.UserTopicEntity;
import com.task.repository.SubTopicMasterRepository;
import com.task.repository.TopicRepository;
import com.task.repository.UserRepository;
import com.task.repository.UserTopicRepository;

@Service
public class UserTopicImpl {

	@Autowired
	private UserTopicRepository userTopicRepository;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private TopicRepository topicRepository;

	@Autowired
	private SubTopicMasterRepository subTopicRepository;

	public UserTopicAddDto assignTopicToUser(Long userId, UserTopicAddDto userTopicAddDto) throws Exception {

		for (int j = 0; j < userTopicAddDto.getTopicId().size(); j++) {
			long topicId = userTopicAddDto.getTopicId().get(j);

			UserEntity userEntity = this.userRepository.findById(userTopicAddDto.getUserId())
					.orElseThrow(() -> new ResourceNotFoundException("User Not Found"));

			TopicEntity topicEntity = this.topicRepository.findById(topicId)
					.orElseThrow(() -> new ResourceNotFoundException("Topic Does Not Found"));

			UserTopicEntity userTopicEntity = this.userTopicRepository
					.findByUserIdAndTopicId(userTopicAddDto.getUserId(), userTopicAddDto.getTopicId().get(j));

			if (userTopicEntity != null) {
				throw new ResourceNotFoundException("user topic already added");
			}

			ArrayList<SubTopicMasterEntity> subTopicMasterEntity = subTopicRepository.findAllByTopicId(topicId);

			ArrayList<UserTopicEntity> userTopicEntityList = new ArrayList<UserTopicEntity>();

			UserTopicEntity newTopicEntity = new UserTopicEntity();
			newTopicEntity.setUser(userEntity);
			newTopicEntity.setTopic(topicEntity);
			newTopicEntity.setStatus(Status.TO_DO.value);
			newTopicEntity.setAssignedBy(userId);

			userTopicEntityList.add(newTopicEntity);
			for (int i = 0; i < subTopicMasterEntity.size(); i++) {

				UserTopicEntity newEntity = new UserTopicEntity();
				newEntity.setUser(userEntity);
				newEntity.setTopic(topicEntity);
				newEntity.setStatus(Status.TO_DO.value);
				newEntity.setSubTopic(subTopicMasterEntity.get(i).getId());
				newTopicEntity.setAssignedBy(userId);

				userTopicEntityList.add(newEntity);
			}
			userTopicRepository.saveAll(userTopicEntityList);
		}

		return userTopicAddDto;

	}

}
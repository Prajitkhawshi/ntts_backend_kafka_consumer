
package com.task.service;

import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.messaging.MessagingException;
import org.springframework.stereotype.Service;

import com.task.entity.UserEntity;

@Service
public class EmailService {

	@Autowired
	private JavaMailSender javaMailSender;

	public void sendSimpleMessage(String emailTo, String subject, String text)
			throws MessagingException, javax.mail.MessagingException {

		MimeMessage message = javaMailSender.createMimeMessage();
		MimeMessageHelper helper = new MimeMessageHelper(message);

		helper.setFrom("noreplyntts@gmail.com");
		helper.setTo(emailTo);
		helper.setSubject(subject);
		helper.setText(text, true);
		javaMailSender.send(message);
	}

	public String sendMail(String emailTo, String subject, String text, UserEntity userEntity) {
		SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
		simpleMailMessage.setFrom("noreplyntts@gmail.com");
		simpleMailMessage.setTo(userEntity.getEmail());
		// simpleMailMessage.setSubject("Apply sucessfully");
		simpleMailMessage.setText(text);
		javaMailSender.send(simpleMailMessage);
		return "Email Send";
	}

	public int generateOTP() {

		int min = 100000;
		int max = 999999;

		int randomInt = (int) Math.floor(Math.random() * (max - min + 1) + min);
		return randomInt;

	}

}

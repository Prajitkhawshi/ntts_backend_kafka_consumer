package com.task.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.task.entity.TechnologyMasterEntity;
import com.task.entity.TopicEntity;
import com.task.entity.UserEntity;
import com.task.entity.UserTechnologyEntity;
import com.task.entity.UserTopicAddDto;
import com.task.repository.TechnologyMasterRepository;
import com.task.repository.TopicRepository;
import com.task.repository.UserTechnologyRepository;

@Service
public class UserServiceImpl {

	@Autowired
	private TechnologyMasterRepository technologyMasterRepository;

	@Autowired
	private UserTechnologyRepository userTechnologyRepository;

	@Autowired
	private TopicRepository topicRepository;

	@Autowired
	private UserTopicImpl userTopicImpl;

	public void setTechnologyAndTopics(UserEntity userEntity, ArrayList<Long> technologys, Long createdById)
			throws Exception {
		List<TechnologyMasterEntity> technologymasterEntity = technologyMasterRepository.findByIdIn(technologys);

		ArrayList<UserTechnologyEntity> userTechnologysList = new ArrayList<>();

		for (int i = 0; i < technologymasterEntity.size(); i++) {
			UserTechnologyEntity userTechnologyList = new UserTechnologyEntity();
			userTechnologyList.setUser(userEntity);
			userTechnologyList.setTechnologyMasterEntity(technologymasterEntity.get(i));
			userTechnologysList.add(userTechnologyList);
		}

		userTechnologyRepository.saveAll(userTechnologysList);

		List<TopicEntity> topicsList = this.topicRepository.findTopicByTechnologyMasterEntityIdIn(technologys);

		ArrayList<Long> ids = new ArrayList<Long>();

		for (int j = 0; j < topicsList.size(); j++) {
			ids.add(topicsList.get(j).getId());
		}

		UserTopicAddDto userTopicAdd = new UserTopicAddDto();

		userTopicAdd.setUserId(userEntity.getId());

		userTopicAdd.setTopicId(ids);

		userTopicImpl.assignTopicToUser(createdById, userTopicAdd);
	}

}

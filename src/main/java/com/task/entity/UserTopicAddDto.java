package com.task.entity;

import java.util.ArrayList;

public class UserTopicAddDto {

	private Long userId;

	private ArrayList<Long> topicId;

	public UserTopicAddDto() {
		// TODO Auto-generated constructor stub
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public ArrayList<Long> getTopicId() {
		return topicId;
	}

	public void setTopicId(ArrayList<Long> topicId) {
		this.topicId = topicId;
	}

	public UserTopicAddDto(Long userId, ArrayList<Long> topicId) {
		super();
		this.userId = userId;
		this.topicId = topicId;

	}

}

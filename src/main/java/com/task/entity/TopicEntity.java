package com.task.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.annotations.Where;

@Entity
@Table(name = "topic_master")
@Where(clause = "is_active=true")
@SQLDelete(sql = "UPDATE topic_master SET is_active=false WHERE id=?")
public class TopicEntity {
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "topic_name")
	private String topicname;

	@Column(name = "description")
	private String description;

	@Column(name = "priority")
	private int priority;

	@Column(name = "is_common")
	private boolean isCommon = false;

	@Column(name = "topic_url")
	private String topicUrl;

	@Column(name = "is_active")
	private boolean isActive = true;

	@Column(name = "created_at")
	@CreationTimestamp
	private Date createdAt;

	@Column(name = "updated_at")
	@UpdateTimestamp
	private Date updatedAt;

	@Column(name = "file_id")
	private Long fileId;

	@Column(name = "created_by")
	private Long createdBy;

	@Column(name = "updated_by")
	private Long updatedBy;

	@ManyToOne
	private TechnologyMasterEntity technologyMasterEntity;

	public TopicEntity() {
		super();

	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public String getDescription() {
		return description;
	}

	public Long getFileId() {
		return fileId;
	}

	public Long getId() {
		return id;
	}

	public int getPriority() {
		return priority;
	}

	public String getTopicname() {
		return topicname;
	}

	public String getTopicUrl() {
		return topicUrl;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public Long getUpdatedBy() {
		return updatedBy;
	}

	public boolean isActive() {
		return isActive;
	}

	public boolean isCommon() {
		return isCommon;
	}

	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

	public void setCommon(boolean isCommon) {
		this.isCommon = isCommon;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public TechnologyMasterEntity getTechnologyMasterEntity() {
		return technologyMasterEntity;
	}

	public void setTechnologyMasterEntity(TechnologyMasterEntity technologyMasterEntity) {
		this.technologyMasterEntity = technologyMasterEntity;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setFileId(Long fileId) {
		this.fileId = fileId;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}

	public void setTopicname(String topicname) {
		this.topicname = topicname;
	}

	public void setTopicUrl(String topicUrl) {
		this.topicUrl = topicUrl;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public void setUpdatedBy(Long updatedBy) {
		this.updatedBy = updatedBy;
	}

	@Override
	public String toString() {
		return "TopicEntity [id=" + id + ", topicname=" + topicname + ", description=" + description + ", priority="
				+ priority + ", isCommon=" + isCommon + ", topicUrl=" + topicUrl + ", isActive=" + isActive
				+ ", createdAt=" + createdAt + ", updatedAt=" + updatedAt + ", fileId=" + fileId + ", createdBy="
				+ createdBy + ", updatedBy=" + updatedBy + ", technologyMasterEntity=" + technologyMasterEntity + "]";
	}

}
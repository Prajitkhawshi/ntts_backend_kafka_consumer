package com.task.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class BulkUpload {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private Long userId;

	private String description;

	private UserBulkUploadStatus status = UserBulkUploadStatus.IN_PROGRESS;

	private String fileName;

	private Long moduleId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public UserBulkUploadStatus getStatus() {
		return status;
	}

	public void setStatus(UserBulkUploadStatus status) {
		this.status = status;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public Long getModuleId() {
		return moduleId;
	}

	public void setModuleId(Long moduleId) {
		this.moduleId = moduleId;
	}

	public BulkUpload(Long id, Long userId, String description, UserBulkUploadStatus status, String fileName,
			Long moduleId) {
		super();
		this.id = id;
		this.userId = userId;
		this.description = description;
		this.status = status;
		this.fileName = fileName;
		this.moduleId = moduleId;
	}

	public BulkUpload() {
		super();
		// TODO Auto-generated constructor stub
	}

}

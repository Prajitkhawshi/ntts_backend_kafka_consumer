package com.task.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "topic_bulk_upload_temp")
public class TopicBulkUploadTempEntity implements Serializable {
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "topic_name")
	private String topicname;

	@Column(name = "description")
	private String description;

	@Column(name = "priority")
	private int priority;

	@Column(name = "topic_url")
	private String topicUrl;

	@Column(name = "bulk_upload_id")
	private Long bulkUploadId;

	private Boolean status;
	private Long technologyMasterEntity;

	public Long getTechnologyMasterEntity() {
		return technologyMasterEntity;
	}

	public void setTechnologyMasterEntity(Long technologyMasterEntity) {
		this.technologyMasterEntity = technologyMasterEntity;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTopicname() {
		return topicname;
	}

	public void setTopicname(String topicname) {
		this.topicname = topicname;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getPriority() {
		return priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}

	public String getTopicUrl() {
		return topicUrl;
	}

	public void setTopicUrl(String topicUrl) {
		this.topicUrl = topicUrl;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public TopicBulkUploadTempEntity() {
		super();
		// TODO Auto-generated constructor stub
	}

	public TopicBulkUploadTempEntity(Long id, String topicname, String description, int priority, String topicUrl,
			Boolean status) {
		super();
		this.id = id;
		this.topicname = topicname;
		this.description = description;
		this.priority = priority;
		this.topicUrl = topicUrl;
		this.status = status;
	}

	public Long getBulkUploadId() {
		return bulkUploadId;
	}

	public void setBulkUploadId(Long bulkUploadId) {
		this.bulkUploadId = bulkUploadId;
	}

}

package com.task.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.annotations.Where;

@Entity
@SQLDelete(sql = "UPDATE Sub_Topic_Master SET is_active=false WHERE id=?")
@Where(clause = "is_active=true")
@Table(name = "Sub_Topic_Master")
public class SubTopicMasterEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@Column(name = "subTopicName")
	private String subTopicName;

	private String description;

	@ManyToOne
	@JoinColumn(name = "topicId")
	private TopicEntity topicId;

	@Column(name = "url")
	private String url;

	@Column(name = "priority")
	private int priority;

	@CreationTimestamp
	private Date createdAt;

	@UpdateTimestamp
	private Date updatedAt;

	@Column(name = "is_active")
	private boolean isActive = true;

	@Column(name = "file_id")
	private Long fileId;

	@Column(name = "created_by")
	private Long createdBy;

	@Column(name = "updated_by")
	private Long updatedBy;

	public SubTopicMasterEntity() {
		super();
		// TODO Auto-generated constructor stub
	}

	public SubTopicMasterEntity(long id, String subTopicName, String description, TopicEntity topicId, String url,
			int priority, Date createdAt, Date updatedAt, boolean isActive, Long fileId, Long createdBy,
			Long updatedBy) {
		super();
		this.id = id;
		this.subTopicName = subTopicName;
		this.description = description;
		this.topicId = topicId;
		this.url = url;
		this.priority = priority;
		this.createdAt = createdAt;
		this.updatedAt = updatedAt;
		this.isActive = isActive;
		this.fileId = fileId;

		this.createdBy = createdBy;
		this.updatedBy = updatedBy;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public String getDescription() {
		return description;
	}

	public Long getFileId() {
		return fileId;
	}

	public long getId() {
		return id;
	}

	public int getPriority() {
		return priority;
	}

	public String getSubTopicName() {
		return subTopicName;
	}

	public TopicEntity getTopicId() {
		return topicId;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public Long getUpdatedBy() {
		return updatedBy;
	}

	public String getUrl() {
		return url;
	}

	public boolean isActive() {
		return isActive;
	}

	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setFileId(Long fileId) {
		this.fileId = fileId;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}

	public void setSubTopicName(String subTopicName) {
		this.subTopicName = subTopicName;
	}

	public void setTopicId(TopicEntity topicId) {
		this.topicId = topicId;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public void setUpdatedBy(Long updatedBy) {
		this.updatedBy = updatedBy;
	}

	public void setUrl(String url) {
		this.url = url;
	}

}

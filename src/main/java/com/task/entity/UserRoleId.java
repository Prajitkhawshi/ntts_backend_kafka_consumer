package com.task.entity;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Embeddable;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Embeddable
public class UserRoleId implements Serializable {

	private static final long serialVersionUID = 1L;

	@ManyToOne
	@JsonManagedReference
	private UserEntity users;

	public UserRoleId() {
		super();
		// TODO Auto-generated constructor stub
	}

	public UserRoleId(UserEntity users, RoleEntity roles) {
		super();
		this.users = users;
		this.roles = roles;
	}

	@ManyToOne
	public RoleEntity getRoles() {
		return roles;
	}

	public void setRoles(RoleEntity roles) {
		this.roles = roles;
	}

	public UserEntity getUsers() {
		return users;
	}

	public void setUsers(UserEntity users) {
		this.users = users;
	}

	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JsonManagedReference
	private RoleEntity roles;
}

package com.task.entity;

import java.util.AbstractMap;
import java.util.Map.Entry;

public enum UserBulkUploadStatus {

	IN_PROGRESS("IN_PROGRESS", 0), PARTIALLY_UPLOADED("PARTIALLY_UPLOADED", 1), UPLOADED("UPLOADED", 2);

	public final String key;
	public final int value;

	UserBulkUploadStatus(String key, int value) {
		this.key = key;
		this.value = value;
	}

	public String getKey() {
		return key;
	}

	public Integer getValue() {
		return value;
	}

	public Entry<String, Integer> getBoth() {
		return new AbstractMap.SimpleEntry<>(key, value);
	}

}

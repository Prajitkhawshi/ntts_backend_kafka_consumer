package com.task.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.AssociationOverride;
import javax.persistence.AssociationOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.annotations.Where;

@Entity
@Table(name = "user_role")
@Where(clause = "is_active=true")
//@Where(clause = "is_active=true")
//@SQLDelete annotation to override the delete command.
//@SQLDelete(sql = "UPDATE user_role u SET is_active=false WHERE u.role_id=? AND u.user_id=?")

@AssociationOverrides({ @AssociationOverride(name = "pk.users", joinColumns = @JoinColumn(name = "user_id")),
		@AssociationOverride(name = "pk.roles", joinColumns = @JoinColumn(name = "role_id")) })
public class UserRoleEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private UserRoleId pk = new UserRoleId();

	@CreationTimestamp
	@Column(name = "created_at")
	private Date createdAt;

	@UpdateTimestamp
	@Column(name = "updated_at")
	private Date updatedAt;

	@Column(name = "is_active")
	private boolean isActive = true;

	public UserRoleEntity() {
		super();
		// TODO Auto-generated constructor stub
	}

	public UserRoleEntity(UserRoleId pk, Date createdAt, Date updatedAt, boolean isActive) {
		super();
		this.pk = pk;
		this.createdAt = createdAt;
		this.updatedAt = updatedAt;
		this.isActive = isActive;
	}

	public UserRoleId getPk() {
		return pk;
	}

	public String getRoleName() {
		return pk.getRoles().getRoleName();
	}

	public Long getRoleId() {
		return pk.getRoles().getId();
	}

	public void setPk(UserRoleId pk) {
		this.pk = pk;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public boolean isActive() {
		return isActive;
	}

	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

}

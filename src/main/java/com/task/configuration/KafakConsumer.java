package com.task.configuration;

import java.util.ArrayList;
import java.util.UUID;

import org.apache.kafka.common.errors.ResourceNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.annotation.KafkaListener;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.task.entity.BulkUpload;
import com.task.entity.GenderEnum;
import com.task.entity.MailTemplate;
import com.task.entity.RoleEntity;
import com.task.entity.TechnologyMasterEntity;
import com.task.entity.TopicBulkUploadTempEntity;
import com.task.entity.TopicEntity;
import com.task.entity.UserEntity;
import com.task.entity.UserRoleEntity;
import com.task.entity.UserRoleId;
import com.task.entity.UserTemporaryEntity;
import com.task.repository.MailRepository;
import com.task.repository.RoleRepository;
import com.task.repository.TechnologyMasterRepository;
import com.task.repository.TopicBulkUploadTempRepository;
import com.task.repository.TopicRepository;
import com.task.repository.UserBulkUploadRepository;
import com.task.repository.UserRepository;
import com.task.repository.UserRoleRepository;
import com.task.repository.UserTemporaryRepository;
import com.task.service.EmailService;
import com.task.service.InviteServiceImpl;
import com.task.service.UserServiceImpl;

@Configuration
@EnableKafka
public class KafakConsumer {
	private static final Logger LOG = LoggerFactory.getLogger(KafakConsumer.class);
	@Autowired
	private UserBulkUploadRepository userBulkUploadRepository;

	@Autowired
	private TopicRepository topicRepository;

	@Autowired
	TopicBulkUploadTempRepository topicBulkUploadTempRepository;

	@Autowired
	TechnologyMasterRepository technologyMasterRepository;

	@Autowired
	private ObjectMapper objectMapper;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private UserTemporaryRepository userTemporaryRepository;

	@Autowired
	private RoleRepository roleRepository;

	@Autowired
	private UserRoleRepository userRoleRepository;

	@Autowired
	private UserServiceImpl userServiceImpl;

	@Autowired
	private InviteServiceImpl inviteServiceImpl;

	@Autowired
	private MailRepository mailRepository;

	@Autowired
	private EmailService emailService;

	@Value("${url}")
	private String frontendURL;

	@KafkaListener(topics = "topicBulk", groupId = "bulkUpload")
	public void topicBulk(String message) throws JsonMappingException, JsonProcessingException {
		System.err.println("Consumer Calling ");

		TopicBulkUploadTempEntity topicBulkUploadTempEntity = objectMapper.readValue(message,
				TopicBulkUploadTempEntity.class);

		TopicEntity topicList = this.topicRepository.findByTopicnameIgnoreCaseAndTechnologyMasterEntityId(
				topicBulkUploadTempEntity.getTopicname(), topicBulkUploadTempEntity.getTechnologyMasterEntity());

		BulkUpload bulkUpload = userBulkUploadRepository.findById(topicBulkUploadTempEntity.getBulkUploadId())
				.orElseThrow();

		UserEntity createdBy = this.userRepository.findById(bulkUpload.getUserId()).orElseThrow();

		if (topicList == null) {
			TopicEntity topicEntity = new TopicEntity();
			topicEntity.setTopicname(topicBulkUploadTempEntity.getTopicname());
			topicEntity.setTopicUrl(topicBulkUploadTempEntity.getTopicUrl());

			topicEntity.setPriority(topicBulkUploadTempEntity.getPriority());

			TechnologyMasterEntity technologyMasterEntity = this.technologyMasterRepository
					.findById(topicBulkUploadTempEntity.getTechnologyMasterEntity()).get();
			topicEntity.setTechnologyMasterEntity(technologyMasterEntity);
			topicEntity.setDescription(topicBulkUploadTempEntity.getDescription());
			topicEntity.setCreatedBy(createdBy.getId());
			this.topicRepository.save(topicEntity);
			topicBulkUploadTempEntity.setStatus(true);
			topicBulkUploadTempRepository.save(topicBulkUploadTempEntity);
		} else {
			topicBulkUploadTempEntity.setStatus(false);
			topicBulkUploadTempRepository.save(topicBulkUploadTempEntity);
			System.err.println("Consumer Calling Done ");
		}

	}

	@KafkaListener(topics = "userBulk", groupId = "bulkUpload")
	public void userBulk(String string) throws JsonMappingException, JsonProcessingException {

		try {

			UserTemporaryEntity userTempObject = objectMapper.readValue(string, UserTemporaryEntity.class);

			BulkUpload bulkUpload = userBulkUploadRepository.findById(userTempObject.getBulkUploadId()).orElseThrow();

			UserEntity databaseName = this.userRepository.findByEmailIgnoreCase(userTempObject.getEmail());
			UserEntity createdBy = this.userRepository.findById(bulkUpload.getUserId()).orElseThrow();

			if (databaseName == null) {

				UserEntity userEntity = new UserEntity();
				userEntity.setName(userTempObject.getName());
				userEntity.setEmail(userTempObject.getEmail().toLowerCase());
				userEntity.setGender(Enum.valueOf(GenderEnum.class, userTempObject.getGender()));
				userEntity.setStatus(0);
				userEntity.setPhoneNumber(userTempObject.getPhone().toString());
				userEntity.setCreatedBy(createdBy);
				userEntity.setDateOfJoining(userTempObject.getDateOfJoining());
				this.userRepository.save(userEntity);

				// Set the status in bulk upload as true
				userTempObject.setStatus(true);
				userTemporaryRepository.save(userTempObject);

				LOG.debug("User entity saved");

				RoleEntity roleEntity = this.roleRepository.findByRoleNameIgnoreCase(userTempObject.getRole());

				if (null == roleEntity) {
					throw new ResourceNotFoundException("Role not found");
				}

				UserRoleEntity userRoleEntity = new UserRoleEntity();
				UserRoleId userRoleId = new UserRoleId(userEntity, roleEntity);
				userRoleEntity.setPk(userRoleId);

				this.userRoleRepository.save(userRoleEntity);

				LOG.debug("User role saved");

				Long techLong = technologyMasterRepository.getTechnologyId(userTempObject.getTechnology());
				ArrayList<Long> list = new ArrayList<>();
				list.add(techLong);
				userServiceImpl.setTechnologyAndTopics(userEntity, list, createdBy.getId());

				LOG.debug("User technology saved");

				UUID uuid = UUID.randomUUID();

				inviteServiceImpl.add(uuid, userEntity.getId());
				String url = frontendURL + uuid;

				LOG.debug("URL generated");

				MailTemplate mailtemplate = mailRepository.findBytemplatename("onboardingTemplate");
				if (null != mailtemplate) {
					String template = mailtemplate.getMailtemp();
					String replaceString = template.replace("USER_NAME", userEntity.getName()).replace("ONBOARDING_URL",
							url);

					this.emailService.sendSimpleMessage(userEntity.getEmail(), "Onboarding - Welcome to NTTS !!",
							replaceString);

					LOG.debug("Mail sent");

				} else {
					throw new ResourceNotFoundException("mailtemplate not found");
				}

			} else {
				userTempObject.setStatus(false);
				userTemporaryRepository.save(userTempObject);
			}
		} catch (Exception e) {
			// TODO: handle exception
			LOG.error("Exception !! " + e.getMessage());
		}

	}

	@KafkaListener(topics = "userBulk", groupId = "bulkUpload")
	public void checkToString(String string) throws JsonMappingException, JsonProcessingException {

		{
			Object readValue = objectMapper.readValue(string, Object.class);

			System.err.println("Kakfa listener");
			System.err.println("Kafka " + readValue.toString());

		}

	}

}

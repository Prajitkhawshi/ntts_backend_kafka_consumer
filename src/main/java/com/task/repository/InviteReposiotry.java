package com.task.repository;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.task.entity.InviteEntity;

public interface InviteReposiotry extends JpaRepository<InviteEntity, Long> {

	@Query(value = "select * from invite_entity where code=:id", nativeQuery = true)
	InviteEntity findbyUuid(@Param("id") UUID id);

}

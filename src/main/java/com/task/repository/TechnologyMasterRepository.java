package com.task.repository;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.task.entity.TechnologyMasterEntity;

@Repository
public interface TechnologyMasterRepository extends JpaRepository<TechnologyMasterEntity, Long> {

	List<TechnologyMasterEntity> findByIdIn(ArrayList<Long> technolgoyId);

	TechnologyMasterEntity findByTechnologyNameIgnoreCase(String technologyName);

	@Query(value = "select id from technology_master where technology_name=:technology", nativeQuery = true)
	Long getTechnologyId(@Param("technology") String technology);

}

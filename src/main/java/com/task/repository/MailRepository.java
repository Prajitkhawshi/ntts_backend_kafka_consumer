package com.task.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.task.entity.MailTemplate;

@Repository
public interface MailRepository extends JpaRepository<MailTemplate, Long> {

//	   @Query(value ="select * from mail_template where templatename=:templatename",nativeQuery=true)
	MailTemplate findBytemplatename(String templatename);

	boolean existsByTemplatename(String templatename);

}

package com.task.repository;

import java.util.ArrayList;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.task.entity.UserEntity;

@Repository
public interface UserRepository extends JpaRepository<UserEntity, Long> {

	@Query(value = "select * from users order by id DESC", nativeQuery = true)
	Page<UserEntity> getAllUserMentorList(Pageable page, Class<UserEntity> list);

	ArrayList<UserEntity> findAllById(Long mentorId);

	UserEntity findByEmail(String email);

	UserEntity findByEmailIgnoreCase(String email);

	UserEntity findByEmailAndIsActiveTrue(String email);
}

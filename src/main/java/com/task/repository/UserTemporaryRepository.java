package com.task.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.task.entity.UserTemporaryEntity;

public interface UserTemporaryRepository extends JpaRepository<UserTemporaryEntity, Long> {

	List<UserTemporaryEntity> findByBulkUploadId(Long bulkUplaodId);

}

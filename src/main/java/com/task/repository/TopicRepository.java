package com.task.repository;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.task.entity.TopicEntity;

@Repository
public interface TopicRepository extends JpaRepository<TopicEntity, Long> {

	List<TopicEntity> findByIdIn(ArrayList<Long> topicId);

	TopicEntity findByTopicnameIgnoreCaseAndTechnologyMasterEntityId(String topicName, Long id);

	List<TopicEntity> findTopicByTechnologyMasterEntityIdIn(ArrayList<Long> technologyId);

}

package com.task.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.task.entity.BulkUpload;

@Repository
public interface UserBulkUploadRepository extends JpaRepository<BulkUpload, Long> {

}

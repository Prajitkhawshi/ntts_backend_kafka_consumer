package com.task.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.task.entity.UserTopicEntity;

@Repository
public interface UserTopicRepository extends JpaRepository<UserTopicEntity, Long> {

	UserTopicEntity findByUserIdAndTopicId(Long userId, Long long1);

}

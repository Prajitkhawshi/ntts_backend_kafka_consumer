package com.task.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.task.entity.TopicBulkUploadTempEntity;

public interface TopicBulkUploadTempRepository extends JpaRepository<TopicBulkUploadTempEntity, Long> {

	List<TopicBulkUploadTempEntity> findByBulkUploadId(Long bulkUploadId);

}

package com.task.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.task.entity.UserTechnologyEntity;

public interface UserTechnologyRepository extends JpaRepository<UserTechnologyEntity, Long> {

	List<UserTechnologyEntity> findByUserId(Long id);

	@Query(value = "select * from user_technology u inner join users us on us.id=u.user_id\r\n"
			+ " and u.is_active=true and us.is_active=true where u.technology_id=:id", nativeQuery = true)
	List<UserTechnologyEntity> findByTech(long id);

}
package com.task.repository;

import java.util.ArrayList;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.task.entity.SubTopicMasterEntity;

@Repository
public interface SubTopicMasterRepository extends JpaRepository<SubTopicMasterEntity, Long> {

	@Query(value = "SELECT * FROM sub_topic_master s WHERE s.topic_id=:id", nativeQuery = true)
	ArrayList<SubTopicMasterEntity> findAllByTopicId(@Param("id") Long id);

}
